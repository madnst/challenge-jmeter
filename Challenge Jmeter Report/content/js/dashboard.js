/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 93.33333333333333, "KoPercent": 6.666666666666667};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.575, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "5. Get Seller Product ID"], "isController": false}, {"data": [0.0, 500, 1500, "7. Get Buyer Product"], "isController": false}, {"data": [1.0, 500, 1500, "10. Get  Buyer Order"], "isController": false}, {"data": [0.0, 500, 1500, "8. Get Buyer Product ID"], "isController": false}, {"data": [0.8, 500, 1500, "4. Get Seller Product"], "isController": false}, {"data": [0.4, 500, 1500, "9. Post Buyer Order"], "isController": false}, {"data": [1.0, 500, 1500, "11. Get Buyer Order ID"], "isController": false}, {"data": [0.6, 500, 1500, "12. Put Buyer Order ID"], "isController": false}, {"data": [0.75, 500, 1500, "2. Post Auth Login"], "isController": false}, {"data": [0.9, 500, 1500, "6. Delete Seller Product ID"], "isController": false}, {"data": [0.45, 500, 1500, "1. Post Auth Register"], "isController": false}, {"data": [0.0, 500, 1500, "3. Post Seller Product"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 120, 8, 6.666666666666667, 5437.799999999999, 268, 68951, 453.0, 10012.500000000011, 47345.54999999995, 67536.43999999994, 1.314247538523881, 615.4439294598169, 25.536798674389697], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["5. Get Seller Product ID", 10, 0, 0.0, 326.2, 286, 484, 301.0, 474.1, 484.0, 484.0, 1.9164430816404754, 1.3905441500574933, 0.6662634151015715], "isController": false}, {"data": ["7. Get Buyer Product", 10, 0, 0.0, 5839.700000000001, 2308, 15381, 4652.0, 14865.100000000002, 15381.0, 15381.0, 0.5633168093735917, 232.60968394406265, 0.22224608494817485], "isController": false}, {"data": ["10. Get  Buyer Order", 10, 0, 0.0, 299.59999999999997, 271, 313, 306.5, 312.8, 313.0, 313.0, 0.20464545175483476, 0.16871258825335106, 0.06934762867082779], "isController": false}, {"data": ["8. Get Buyer Product ID", 10, 0, 0.0, 49081.7, 26983, 68951, 48894.5, 68277.40000000001, 68951.0, 68951.0, 0.12426836997179107, 646.2239453887114, 0.042474540517702025], "isController": false}, {"data": ["4. Get Seller Product", 10, 0, 0.0, 417.20000000000005, 286, 653, 370.5, 647.7, 653.0, 653.0, 1.8705574261129816, 1.3609035961466516, 0.6393506827534605], "isController": false}, {"data": ["9. Post Buyer Order", 10, 4, 40.0, 898.8, 307, 1359, 1128.5, 1356.5, 1359.0, 1359.0, 0.20445716622367613, 0.10931669776119404, 0.08346005418114906], "isController": false}, {"data": ["11. Get Buyer Order ID", 10, 0, 0.0, 300.9, 268, 336, 309.0, 334.3, 336.0, 336.0, 0.20452407248333127, 0.16837284482758622, 0.07010541937661063], "isController": false}, {"data": ["12. Put Buyer Order ID", 10, 4, 40.0, 296.2, 275, 309, 297.0, 309.0, 309.0, 309.0, 0.2044028371113791, 0.1184298859943176, 0.09230065613310712], "isController": false}, {"data": ["2. Post Auth Login", 10, 0, 0.0, 555.5, 337, 966, 539.5, 941.2, 966.0, 966.0, 6.882312456985547, 3.4545982450103234, 2.0364655024088094], "isController": false}, {"data": ["6. Delete Seller Product ID", 10, 0, 0.0, 485.2, 273, 1241, 307.5, 1240.2, 1241.0, 1241.0, 1.9837333862328903, 0.6237911624677643, 0.7322765820273754], "isController": false}, {"data": ["1. Post Auth Register", 10, 0, 0.0, 1392.4, 1279, 1585, 1380.0, 1576.0, 1585.0, 1585.0, 4.173622704507513, 2.380676779006678, 1.7733820690734559], "isController": false}, {"data": ["3. Post Seller Product", 10, 0, 0.0, 5360.2, 3354, 8127, 5265.5, 7878.800000000001, 8127.0, 8127.0, 1.2198097096852891, 0.8231333099536472, 279.47269913393507], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["404/Not Found", 8, 100.0, 6.666666666666667], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 120, 8, "404/Not Found", 8, "", "", "", "", "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["9. Post Buyer Order", 10, 4, "404/Not Found", 4, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": ["12. Put Buyer Order ID", 10, 4, "404/Not Found", 4, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
